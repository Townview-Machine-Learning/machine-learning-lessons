import keras
from keras.datasets import mnist

# Load data
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# Preprocess images
x_train = x_train.reshape(-1, 28, 28, 1)
x_test = x_test.reshape(-1, 28, 28, 1)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

# Load model
model = keras.models.load_model('model.h5')

# Evaluate model

score = model.evaluate(x_test, y_test)

print(f"Loss: {score[0]}")
print(f"Accuracy: {score[1]}")

