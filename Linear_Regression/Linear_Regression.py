import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

# First let's create our data.
# We will create a set of random data for now.
x = np.arange(50)
y = np.cumsum(np.random.randint(1, 50, 50))

# Let's graph to see the data.
plt.scatter(x, y)
plt.show()

# Now let's calculate the slope
num = 0
den = 0
for xi, yi in zip(x, y):
    num += (xi - x.mean())*(yi - y.mean())
    den += (xi - x.mean())**2
a = num/den

# Lastly, let's calculate the intercept
b = y.mean() - a*x.mean()

# Let's print these values
print("slope: " + str(a))
print("y-intercept: " + str(b))

# Now, we can plot the line
plt.scatter(x, y)
plt.plot(x, a*x + b)
plt.show()

# While that algorithm works pretty well, we can get even better results using the model from scikit-learn
model = LinearRegression()

# Now let's train the model on our data
# First we will have to reshape the data a bit
x = x.reshape(1, -1)
y = y.reshape(1, -1)
model.fit(x, y)

# Now we can even score our model
#r2 = model.score(x, y)

# Lastly, let's graph it
plt.scatter(x, y)
plt.plot(x.reshape(-1), model.predict(x).reshape(-1))
plt.show()

