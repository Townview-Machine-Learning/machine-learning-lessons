# Let's start by importing the packages we'll need

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.svm import SVC

# If you are using jupyter notebooks, you will also need to run the following command:
# %matplotlib inline

# For this lesson, we'll be using a dataset about breat cancer patients that we got from Kaggle.com

df = pd.read_csv('dataset.csv')

# Let's figure out some of the details of this dataset to see what we're working with

df.head()

# As you can see, this dataset has a large number of features we can work with

df.info()

# From here, we can tell that there are no null values present in this dataset aside from the last column, which seems to be blank
# We can easily remove this and the id column to make our job easier

df = df.drop([['id', 'Unnamed: 32']])

# Now, we can do the usual train test split with our data

y = df['diagnosis']

X = df.drop(['diagnosis'], axis=1)

# Notice how we just dropped the column we didn't want instead of listing out all the parameters,
# Which is a much more lazy/efficient way of going about this

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.3, random_state = 101)

# Now that we have split our data, let's create and train our model

model = SVC()
model.fit(X_train, y_train)

prediction = model.predict(X_test)

# Now for the final step, let's check the accuracy (as well as the recall and f1 score) of the model

print(classification_report(y_test, prediction))