# Machine Learning Course

Here we will post all of the code and materials for the machine learning lessons.

This course covers these algorithms:
1. Linear Regression
    * Graph
    * Equation
    * Python Implementation
2. Logistic Regression
    * Graph
    * Equation
    * Python Implementation
3. KNNs
    * Visuals/Logic
    * Python Implementation
4. SVMs
    * Visuals/Logic
    * Kernels
    * Python Implementation
5. K Means Clustering
    * Visuals/Logic
    * Python Implementation
6. Neural Networks
    * Introduction
    * Explanation of NN evaluation
    * Subcategories
        * CNN
        * RNN

[Our Club Website](https://www.townviewai.com/) | [Our Cloud Computing Service](http://www.townviewai.com:4200/) |  [Our Discord Server](https://discord.gg/MTChRGn) | [Our YouTube Channel](https://www.youtube.com/channel/UCtoopcfdAcxFliCMCVt93qA) | [Our Twitch Channel](https://www.twitch.tv/townviewml) | [Our Instagram](https://www.instagram.com/townviewml/)
