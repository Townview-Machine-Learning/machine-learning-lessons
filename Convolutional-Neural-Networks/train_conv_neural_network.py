from keras.models import Sequential
from keras.datasets import mnist
from keras.layers import Dense, MaxPooling2D, Conv2D, Flatten

import matplotlib.pyplot as plt

# Import dataset
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# Preview one of the images
plt.imshow(x_train[0], cmap='gray')
plt.show()

# Preprocess images
x_train = x_train.reshape(-1, 28, 28, 1)
x_test = x_test.reshape(-1, 28, 28, 1)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

# Make model
model = Sequential()

model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(28, 28, 1)))
model.add(MaxPooling2D())
model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
model.add(MaxPooling2D())
model.add(Flatten())
model.add(Dense(1000, activation='relu'))
model.add(Dense(10, activation='softmax'))

model.summary()
model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

model.fit(x_train, y_train, epochs=10, validation_data=(x_test, y_test))

model.save("model.h5")

