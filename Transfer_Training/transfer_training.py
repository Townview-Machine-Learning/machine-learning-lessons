from keras.applications import vgg16
from keras.models import Model
from keras.layers import Dense
import numpy as np
import cv2
import os

# Load the images
image_paths = os.listdir("Data")

x_train = []
y_train = []

for path in image_paths:
    image = cv2.imread("Data/" + path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = cv2.resize(image, (224, 224))

    image = image.astype('float32')
    image /= 255

    x_train.append(image)
    
    # dogs are 1 and cats are 0
    if path.startswith('dog'):
        y_train.append(1)
    else:
        y_train.append(0)

x_train = np.asarray(x_train)
y_train = np.asarray(y_train)

# Load model
model = vgg16.VGG16()

# Freeze the layers
for layer in model.layers:
    layer.trainable = False

# Let's replace the output layer with our own
model = Model(inputs=model.inputs, outputs=Dense(1, activation='sigmoid')(model.layers[-2].output))

model.summary()

model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

model.fit(x_train, y_train, epochs=15)

model.save('model.h5')

