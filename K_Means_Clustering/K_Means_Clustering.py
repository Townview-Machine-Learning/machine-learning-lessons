import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import random

# This method will be used later, but does not need to be understood.
# It simply generates a random color.
def create_color():
    chars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f']
    color = "#"
    for x in range(6):
        color += random.choice(chars)
    return color


# Let's create some random data
data = np.random.uniform(0, 10, (100, 2))

# Let's see what the data looks like
plt.plot(data[:,0], data[:,1], "o")
plt.show()

# Next we can create and train our model with the number of clusters that we like
model = KMeans(n_clusters=5)

model.fit(data)

# Now let's display the newly created classes with 1000 points. This will allow us to see the boundaries between the classes.
colors = {cls: create_color() for cls in range(model.n_clusters)}
for x in range(1000):
    test_point = np.random.uniform(0, 10, (1, 2))
    plt.plot(test_point[0][0], test_point[0][1], "o", color=colors[model.predict(test_point)[0]])

for point in model.cluster_centers_:
    plt.plot(point[0], point[1], "v", color="#000000")
plt.show()

