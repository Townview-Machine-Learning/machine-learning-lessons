# First, let's import all the packages we'll need

import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, LSTM
from sklearn.metrics import classification_report

# We'll be using a different mnist dataset (one that has pieces of clothing as opposed to handwritten numerals) for this lecture, but this time we'll be using RNNs (more specifically LSTMs) to get our results
# Just like last time, we can import in our dataset, split it between training and testing, and normalize the values

mnist = tf.keras.datasets.fashion_mnist.load_data()
(x_train, y_train),(x_test, y_test) = mnist

x_train = x_train/255.0
x_test = x_test/255.0

# Now, we can create our model and add layers

model = Sequential()

# Now, we can add our other hidden layers using the LSTM layer type

model.add(LSTM(256, input_shape = (28,28), activation = 'relu', return_sequences = True))

model.add(LSTM(128, activation = 'relu'))

model.add(Dense(10, activation='softmax'))

# Now, we can compile our model

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['acc'])

# and then, we will fit it to our training data

model.fit(x_train, y_train, epochs=3, validation_data=(x_test, y_test))

# Finally, we can use our model to predict the results for x_test

prediction = model.predict(x_test)

# Let's print out the classification report to see how well our model was able to predict those values

print(classification_report(y_test, prediction))