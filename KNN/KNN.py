import pandas as pd
import math

# Load the dataset
x = pd.read_csv("Iris.csv", header=None)

# Create a sample data point
test = [2.1, 4.8, 8, 3.3]

# We can use iloc, but we have to use 1 as the first index because 0 is the column labels.
# Also, the last element of each series is the class for the point.

# Let's calculate the distances to each point.
distances = []

for i in range(1, len(x)):
    distance = 0
    for j in range(4):
        distance += (float(test[j]) - float(x.iloc[i][j+1]))**2
    distance = math.sqrt(distance)
    distances.append((distance, i))

# Now let's sort the values and get the first k values.  
distances = sorted(distances)
# We will set k to 3 for now. We should use odd numbers to ensure there is no tie in the votes.
k = 3
distances = distances[:k]

# Now we have to count the votes.
votes = {}

for i in range(k):
    species = x.iloc[distances[i][1]].iloc[-1]
    if species in votes:
        votes[species] += 1
    else:
        votes[species] = 1

print(max(votes, key=votes.get))

