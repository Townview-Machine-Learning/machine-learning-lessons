# Let's start by importing the packages we'll need

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report

# If you are using jupyter notebooks, you will also need the following line:

# %matplotlib inline

# In our example for linear regression, we used numpy arrays to create our data
# With this lesson, we'll be using real world data from the titanic to determine whether a person would survive based on their features.
# We'll also learn a bit about using seaborn to quickly and easily display data

# First, let's import our dataset to begin

titanic = pd.read_csv('train.csv')

# Now we can use the head, info, and describe functions to get a sense of what kind of dataset we're working with

titanic.head()
titanic.info()
titanic.describe()

# Now that we have a better sense of our data is, let's figure out how we can get rid of null values
# First, we have to know where our null values are, which we can easily do with the isnull() method
# Let's create a heatmap for our null values

sns.heatmap(titanic.isnull())
plt.show()

# As you can see, the only num values we have are in the Cabin and Age indeces
# Since we will not be using Cabin for our regression, we only need to worry about Age
# There are many ways we can fill these null points, and one of them is to just use the average age for these values

age = round(titanic['Age'].mean())

# Now we have stored the average person's age in a new age variable
# We can use this to replace all the null values we have with the fillna method

titanic['Age'] = titanic['Age'].fillna(age)

# Now let's check our heatmap again

sns.heatmap(titanic.isnull())
plt.show()

# Notice how there aren't any null age columns anymore
# Now we can format our dataset a little better so that all values can either be dropped or converted to numerical form

sex = pd.get_dummies(titanic['Sex'])
embark = pd.get_dummies(titanic['Embarked'], drop_first=True)

# What we have just done is convert the values from the Sex and Embarked column to various columns of numerical data, which we can now concatinate onto our data frame

titanic = pd.concat([titanic, sex, embark], axis=1)

# Now we can drop the columns we don't need

titanic.drop(['Sex', 'Embarked', 'Name', 'Ticket', 'PassengerId', 'Cabin'], axis=1)

# Now that we have finally cleaned our data, we can get to the actual Machine Learning portion of this lesson
# Let's first seperate our data into x for all the columns we want to train with as well as y for the survived column, which we want to predict

x = titanic[['Pclass', 'Age', 'SibSp', 'Parch', 'Fare', 'female', 'male', 'Q', 'S']]
y = titanic['Survived']

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=101)

# Now let's create our logistic regression model

model = LogisticRegression()

# Now we can fit our model with our x_train and y_train

model.fit(x_train, y_train)

# Finally, we can predict our values and check our accuracy metrics

prediction = model.predict(x_test)

print(classification_report(y_test, prediction))

