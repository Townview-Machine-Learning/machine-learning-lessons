from tensorflow.keras.models import Sequential # Our neural network model
from tensorflow.keras.layers import Dense # The dense layer is a layer of densely connected neurons for our network
from tensorflow.keras.utils import to_categorical # This will allow us to create categories for our labels
import tensorflow.keras.datasets.mnist as mnist # Our dataset to train on
import matplotlib.pyplot as plt
import numpy as np

# Let's load our data.
# It is already split up for us, so we just have to distribute it into our variables properly.
((x_train, y_train), (x_test, y_test)) = mnist.load_data()

# Now let's look at one of the images and make sure that the answers match up.
print(y_train[0])
plt.imshow(x_train[0], cmap="gray")
plt.show()

# If we take a look at the image, we will see that it is 28x28
print(x_train[0].shape)

# Let's do some preprocessing on the images so that we can use them.
# We will flatten the images to be a single line. Since they are 28x28, we know that
# they would form a single 784 pixel long array.
x_train = x_train.reshape((-1, 784))
x_test = x_test.reshape((-1, 784))

# Neural networks prefer small numbers, unlike the 0 to 255 values in the images.
# So, since we know the range of numbers let's shrink the range to be between 0 and 1.
x_train = x_train / 255
x_test = x_test / 255

# Now, let's finish the preprocessing with our labels.
# Right now, they are just numbers, but we need to turn them into classes.
# For example, instead of 8, the label should be [0, 0, 0, 0, 0, 0, 0, 1, 0, 0]
# This is called a one-hot format.
temp = []
for label in y_train:
    temp.append(to_categorical(label, num_classes=10)) # We have 10 digits, and the label is to be encoded
y_train = np.array(temp)

temp = []
for label in y_test:
    temp.append(to_categorical(label, num_classes=10))
y_test = np.array(temp)

# Now let's start creating our model
model = Sequential()

model.add(Dense(512, input_shape=(784,), activation='relu'))
model.add(Dense(128, activation='relu'))
model.add(Dense(10, activation='softmax')) # This last layer will tell us what digit is predicted, so we will have 10 neurons, one for each digit.

model.summary() # This will allow us to see a summary of our model.

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['acc']) # This is the last step to create the model.
# Here, we have specified the loss function, categorical crossentropy, which will measure the loss based on categorical outputs.
# Our optimizer is called adam, and that is the most common choice.
# The metric is accuracy, this means that the model will be judged based on its accuracy.

# Now let's start training.
history = model.fit(x_train, y_train, epochs=5, validation_data=(x_test, y_test))

# Now let's take a look at our loss curve.
plt.plot(history.history["loss"])
plt.title("Loss")
plt.xlabel("Epoch")
plt.ylabel("Loss")
plt.show()

# Now let's have the model predict on the testing data
predictions = model.predict(x_test)

# Let's take a look at one of the images and the prediction.
# Let's turn the predictions back into numbers first.
predictions = np.argmax(predictions, axis=1)

print(predictions[0])
plt.imshow(x_test[0].reshape(28, 28), cmap='gray')
plt.show()

