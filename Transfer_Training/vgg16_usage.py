from keras.applications import vgg16
import matplotlib.pyplot as plt
import cv2

# Load model
model = vgg16.VGG16()

# Let's print out a summary of the model
model.summary()

# Let's test it out.

# Load and process image
image = cv2.imread('image.jpg')
image = cv2.resize(image, (224, 224))
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

plt.imshow(image)
plt.show()

image = image.astype('float32')
image /= 255

image = image.reshape(-1, 224, 224, 3)

prediction = model.predict(image)

print(vgg16.decode_predictions(prediction, 10))

